import { Address } from './address.model';
import { Entity } from './entity.model';

export class User extends Entity
{
    username: string;
    roles: string[];
    token: string;
    address: Address;
}