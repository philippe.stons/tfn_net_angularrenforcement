import { Category } from './category.model';
import { Entity } from './entity.model';

export class Product extends Entity
{
    label: string;
    price: number;
    quantity: number;
    category: Category;
}