import { Entity } from './entity.model';

export class Address extends Entity
{
    street: string;
    number: number;
    city: string;
    state: string;
    zipCode: number;
    country: string;
}