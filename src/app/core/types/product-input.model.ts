import { Observable } from 'rxjs';
import { Product } from './../model/prodct.model';

export class ProductInputType
{
    product: Product;
    submitCb: (entity: Product) => Observable<any>;
}