import { AuthService } from './../api/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private router: Router,
    private authService: AuthService
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
    return this.checkPermission(state);
  }

  canActivateChild(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
  {
    return this.checkPermission(state);
  }

  private checkPermission(state: RouterStateSnapshot)
  {
    if(!this.authService.isUser())
    {
      this.authService.setRedirectUrl(state.url);
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
