import { AuthService } from './../api/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate, CanActivateChild {
  constructor(
    private authService: AuthService,
    private router: Router
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
    return this.checkPermission(state);
  }

  canActivateChild(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
  {
    return this.checkPermission(state);
  }

  private checkPermission(state: RouterStateSnapshot)
  {
    if(!this.authService.isAdmin())
    {
      this.authService.setRedirectUrl(state.url);
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }  
}
