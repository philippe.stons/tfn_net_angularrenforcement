import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { User } from './../model/user.model';
import { Router } from '@angular/router';
import { ServerService } from './server.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService
{
    private jwt: JwtHelperService = new JwtHelperService();
    private isLogged: boolean = false;
    private redirectUrl: string;

    constructor(
        private server: ServerService,
        private router: Router
    ){
        this.isLogged = this.checkTokenValidity();
    }

    login(user: User){
        return this.server.post<User>('login', user)
            .pipe(map((user: User) => 
            {
                if(user.token)
                {
                    localStorage.setItem('token', user.token.replace('Bearer ', ''));
                    localStorage.setItem('user', JSON.stringify(user));
                    this.isLogged = true;
                }
                return this.isLogged;
            }))
    }

    logout()
    {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.isLogged = false;
        this.router.navigate(['auth/login']);
    }

    isCurrentUser(user: User) 
    {
        const loggedUser = JSON.parse(localStorage.getItem('user')) as User;

        return user.id === loggedUser.id;
    }

    isUser()
    {
        return this._checkAuthorization('USER');
    }

    isAdmin()
    {
        return this._checkAuthorization("ADMIN");
    }

    private _checkAuthorization(type: string)
    {
        const user = JSON.parse(localStorage.getItem('user')) as User;
        console.log(user);
        
        if(user)
        {
            return user.roles.includes(type);
        }

        return false;
    }

    getLoggedIn()
    {
        return this.isLogged;
    }

    private checkTokenValidity()
    {
        const token = localStorage.getItem('token');

        if(!token || this.jwt.isTokenExpired(token))
        {
            this.logout()
            return false;
        }

        return true;
    }

    setRedirectUrl(url: string)
    {
        this.redirectUrl = url;
    }

    redirect()
    {
        if(this.redirectUrl)
        {
            this.router.navigate([this.redirectUrl]);
        } else 
        {
            this.router.navigate(['product']);
        }
    }
}