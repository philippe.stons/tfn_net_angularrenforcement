import { Product } from './../model/prodct.model';
import { ServerService } from './server.service';
import { Injectable } from '@angular/core';
import { CRUD, CrudConfig } from './crud';

const config: CrudConfig = { path: 'product' }

@Injectable({
    providedIn: 'root'
})
export class ProductService extends CRUD<Product>
{
    constructor(protected server: ServerService)
    {
        super(server, config);
    }
}