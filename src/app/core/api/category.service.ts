import { Category } from './../model/category.model';
import { ServerService } from './server.service';
import { Injectable } from '@angular/core';
import { CRUD, CrudConfig } from './crud';

const config: CrudConfig = { path: 'category' }

@Injectable({
    providedIn: 'root'
})
export class CategoryService extends CRUD<Category>
{
    constructor(protected server: ServerService)
    {
        super(server, config);
    }
}