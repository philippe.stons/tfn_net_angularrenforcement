import { Basket } from './../model/basket.model';
import { UrlTree } from '@angular/router';
import { Product } from './../model/prodct.model';
import { ServerService } from './server.service';
import { Injectable } from '@angular/core';
import { CRUD, CrudConfig } from './crud';

const config: CrudConfig = { path: 'user' }

@Injectable({
    providedIn: 'root'
})
export class UserService extends CRUD<Product>
{
    constructor(protected server: ServerService)
    {
        super(server, config);
    }

    public getBasket()
    {
        return this.server.get<Product[]>(this.config.path + "/basket");
    }

    public updateBasket(basket: Product[])
    {
        const bsk = { basket: basket };
        return this.server.put<Basket>(this.config.path + '/basket/', bsk);
    }
}