import { AdminGuard } from './core/guards/admin.guard';
import { AuthGuard } from './core/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'auth', loadChildren: () => import('./modules/login/login.module').then(t => t.LoginModule) },
  { path: 'product', canActivateChild: [AuthGuard], loadChildren: () => import('./modules/product/product.module').then(t => t.ProductModule) },
  { path: 'user', canActivateChild: [AuthGuard], loadChildren: () => import('./modules/user/user.module').then(t => t.UserModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
