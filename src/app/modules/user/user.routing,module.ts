import { BasketComponent } from './pages/basket/basket.component';
import { UserViewComponent } from './pages/user-view/user-view.component';
import { UserComponent } from './pages/user/user.component';
import { UserListComponent } from './pages/user-list/user-list.component';
import { AdminGuard } from './../../core/guards/admin.guard';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = 
[
    { path: '', component: UserListComponent },
    { path: 'add', component: UserComponent },
    { path: 'basket', component: BasketComponent },
    { path: ':id/edit', component: UserComponent },
    { path: ':id', component: UserViewComponent },
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: 
    [
        RouterModule
    ]
})
export class UserRoutingModule { }