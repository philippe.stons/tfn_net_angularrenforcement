import { MatTableDataSource } from '@angular/material/table';
import { Product } from 'src/app/core/model/prodct.model';
import { UserService } from './../../../../core/api/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {
  basket: Product[];
  displayColumns: string[] = ['label', 'quantity', 'actions'];
  dataSource: MatTableDataSource<Product>;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  private refresh()
  {
    this.userService.getBasket()
      .subscribe((b: Product[]) =>
      {
        this.basket = b;
        this.dataSource = new MatTableDataSource(this.basket);
        console.log(this.basket);
        
      });
  }

  add(product: Product)
  {
    product.quantity++;
  }

  remove(product: Product)
  {
    product.quantity--;
    product.quantity = product.quantity >= 0 ? product.quantity : 0;
  }

  save()
  {
    this.basket = this.basket.filter(
      (product: Product) => 
      {
        return product.quantity > 0;
      }
    );

    this.userService.updateBasket(this.basket)
      .subscribe(() => this.refresh());
  }

  checkout()
  {

  }
}
