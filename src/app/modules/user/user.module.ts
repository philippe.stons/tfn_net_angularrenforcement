import { UserRoutingModule } from './user.routing,module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { BasketComponent } from './pages/basket/basket.component';
import { UserComponent } from './pages/user/user.component';
import { UserViewComponent } from './pages/user-view/user-view.component';
import { UserListComponent } from './pages/user-list/user-list.component';



@NgModule({
  declarations: [
    BasketComponent,
    UserComponent,
    UserViewComponent,
    UserListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UserRoutingModule
  ]
})
export class UserModule { }
