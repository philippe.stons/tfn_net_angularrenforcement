import { AuthService } from './../../../../core/api/auth.service';
import { User } from './../../../../core/model/user.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.userForm = this.fb.group(
      {
        username: [null, [Validators.required]],
        password: [null]
      },
    );
  }

  submit() 
  {
    if(this.userForm.valid)
    {
      const user = this.userForm.value as User;

      console.log(user);
      this.authService.login(user)
        .subscribe(
          (logged) => 
          {
            console.log(logged);
            if(logged) 
            {
              this.authService.redirect();
            }
          }
        );
    }
  }

}
