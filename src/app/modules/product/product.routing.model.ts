import { AdminGuard } from './../../core/guards/admin.guard';
import { ProductViewComponent } from './pages/product-view/product-view.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductComponent } from './pages/product/product.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = 
[
    { path: '', component: ProductListComponent },
    { path: 'add', canActivate: [AdminGuard], component: ProductComponent },
    { path: ':id/edit', canActivate: [AdminGuard], component: ProductComponent },
    { path: ':id', component: ProductViewComponent },
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: 
    [
        RouterModule
    ]
  })
  export class ProductRoutingModule { }