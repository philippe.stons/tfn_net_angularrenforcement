import { AuthService } from './../../../../core/api/auth.service';
import { ProductDialogComponent } from './../../components/product-dialog/product-dialog.component';
import { ProductFormComponent } from './../../components/product-form/product-form.component';
import { Router } from '@angular/router';
import { ProductService } from './../../../../core/api/product.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from 'src/app/core/model/prodct.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  displayColumns: string[] = ["id", 'label', 'category', 'price', 'quantity', 'actions'];
  dataSource: MatTableDataSource<Product>;
  data: Product[];
  isAdmin: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private productService: ProductService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private authService: AuthService
  ) {
    console.log("TEST");
    
    this.isAdmin = this.authService.isAdmin();
  }

  ngOnInit(): void {
    this.refreshData();
  }

  private refreshData()
  {
    this.productService.getAll()
      .subscribe((products: Product[]) => 
      {
        this.data = products;
        this.updateDataSource();    
      })
  }

  private updateDataSource() 
  {
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  create()
  {
    this.router.navigate(['product/add']);
  }

  viewElement(elem: Product)
  {
    this.router.navigate(['product', elem.id]);
  }

  editElement(elem: Product)
  {
    // this.router.navigate(['product', elem.id, 'edit']);
    const dlg = this.dialog.open(ProductDialogComponent, { data: elem });

    dlg.beforeClosed().subscribe(
      (product: Product) => 
      {
        console.log(product);
        this.refreshData();
      }
    );
  }

  deleteElem(elem: Product)
  {
    //this.productService.delete(elem.id);
  }
}
