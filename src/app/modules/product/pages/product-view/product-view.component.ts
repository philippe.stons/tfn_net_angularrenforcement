import { UserService } from './../../../../core/api/user.service';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from './../../../../core/api/product.service';
import { Product } from 'src/app/core/model/prodct.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {
  product: Product;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);

    if(!isNaN(id))
    {
      this.productService.getOneById(id)
        .subscribe(
          (p: Product) => 
          {
            this.product = p;
          }
        )
    }
  }
}
