import { Entity } from './../../../../core/model/entity.model';
import { ProductInputType } from './../../../../core/types/product-input.model';
import { Category } from './../../../../core/model/category.model';
import { Product } from 'src/app/core/model/prodct.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from './../../../../core/api/category.service';
import { ProductService } from './../../../../core/api/product.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productForm: FormGroup;
  edit: boolean = false;
  product: Product;
  categories: Category[];
  productInputType: ProductInputType;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private categoryService: CategoryService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    // this.productForm = this.fb.group(
    //   {
    //     label: [null, [Validators.required]],
    //     category: [null, [Validators.required]],
    //     price: [null, [Validators.required]],
    //     quantity: [null, [Validators.required]],
    //   }
    // );

    // this.categoryService.getAll()
    //   .subscribe((c: Category[]) => this.categories = c);

    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);

    if(!isNaN(id))
    {
      this.edit = true;
      this._getProduct(id);
    } else
    {
      this.productInputType = 
      {
        product: null,
        submitCb: (entity: Product) => this.productService.insert(entity)
      }
    }
  }

  private _getProduct(id: number)
  {
    this.productService.getOneById(id)
      .subscribe(p => 
        {
          this.productInputType = 
          {
            product: p,
            submitCb: (entity: Product) => this.productService.update(entity.id, entity)
          }
          this.product = p;
          this.productForm.patchValue(this.product);
          this.productForm.get('category').setValue(this.product.category.id);
        })
  }

  // submit()
  // {
  //   if(this.productForm.valid)
  //   {
  //     let product = this.productForm.value as Product;
      
  //     for(let cat of this.categories)
  //     {
  //       if(product.category == cat.id as any)
  //       {
  //         product.category = cat;
  //       }
  //     }
  //     console.log(product);

  //     if(!this.edit)
  //     {
  //       this.productService.insert(product)
  //         .subscribe(() => this.router.navigate(['product']));
  //     } else 
  //     {
  //       this.productService.update(this.product.id, product)
  //         .subscribe(() => this.router.navigate(['product']));
  //     }
  //   }
  // }
}
