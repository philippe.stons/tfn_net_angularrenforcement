import { ProductService } from './../../../../core/api/product.service';
import { Product } from 'src/app/core/model/prodct.model';
import { ProductInputType } from './../../../../core/types/product-input.model';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.css']
})
export class ProductDialogComponent implements OnInit {
  productInputType: ProductInputType;

  constructor(
    public dialogRef: MatDialogRef<ProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,
    private productService: ProductService
  ) {
    if(data)
    {
      this.productInputType = 
      {
        product: data,
        submitCb: (entity: Product) => this.productService.update(entity.id, entity)
      }
    }
  }

  ngOnInit(): void {
  }

  closeDialog(product: Product)
  {
    this.dialogRef.close(product);
  }
}
