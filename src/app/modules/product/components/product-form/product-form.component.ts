import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from './../../../../core/api/category.service';
import { ProductService } from './../../../../core/api/product.service';
import { Category } from './../../../../core/model/category.model';
import { ProductInputType } from './../../../../core/types/product-input.model';
import { Product } from './../../../../core/model/prodct.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  productForm: FormGroup;
  edit: boolean;
  product: Product;
  @Input() productType: ProductInputType;
  @Output() productEmitter = new EventEmitter<Product>();
  categories: Category[];

  constructor(
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.productForm = this.fb.group(
      {
        label: [null, [Validators.required]],
        category: [null, [Validators.required]],
        price: [null, [Validators.required]],
        quantity: [null, [Validators.required]],
      }
    );

    this.categoryService.getAll()
      .subscribe((c: Category[]) => this.categories = c);

    // const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    if(this.productType.product)
    {
      this.productForm.patchValue(this.productType.product);
      this.productForm.get('category').setValue(this.productType.product.category.id);
    }
  }

  submit()
  {
    if(this.productForm.valid)
    {
      let product = this.productForm.value as Product;
      product.id = this.productType.product?.id;
  
      for(let cat of this.categories)
      {
        if(product.category == cat.id as any)
        {
          product.category = cat;
        }
      }
      console.log(product);
      
      this.productType.submitCb(product).subscribe(
        () => this.productEmitter.emit(product)
      )
    }
  }
}
