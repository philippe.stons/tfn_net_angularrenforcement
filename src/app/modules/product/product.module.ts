import { ProductRoutingModule } from './product.routing.model';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './pages/product/product.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductViewComponent } from './pages/product-view/product-view.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductDialogComponent } from './components/product-dialog/product-dialog.component';



@NgModule({
  declarations: [
    ProductComponent,
    ProductListComponent,
    ProductViewComponent,
    ProductFormComponent,
    ProductDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule
  ],
  exports:
  [
    ProductFormComponent,
    ProductDialogComponent,
  ]
})
export class ProductModule { }
