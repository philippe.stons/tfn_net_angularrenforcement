import { AuthService } from './core/api/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TFNDemoDotNet';

  constructor(private authService: AuthService)
  {}

  isLogged()
  {
    return this.authService.getLoggedIn();
  }

  logout()
  {
    this.authService.logout();
  }
}
